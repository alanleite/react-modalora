#### 1.1.3 (2018-11-16)

##### Chores

*  fixed travis config

#### 1.1.2 (2018-11-16)

##### Chores

*  improved scripts & added travis ([866a9a72](https://github.com/totvs-store/react-modalora/commit/866a9a72555a3efe191a2a86aab64a74d7cbd667))

#### 1.1.1 (2018-11-16)

##### Bug Fixes

*  problems using MS Edge ([f4f5843a](https://github.com/totvs-store/react-modalora/commit/f4f5843ae0b5eda5a34f6f6f773222bc31400f5f))

## 1.1.0 (2018-11-07)

##### New Features

*  option to be able to close the modal on overlay click ([6ed66382](https://github.com/totvs-store/react-modalora/commit/6ed663824b2d1b43ac388795a3a2cdf141deadfa))

##### Tests

*  added tests for overlay click ([119ec912](https://github.com/totvs-store/react-modalora/commit/119ec91245284a57d6a63ba2b7a97c04658a3f01))
